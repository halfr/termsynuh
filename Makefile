SRC = termsynuh6x11r.bdf termsynuh6x11b.bdf
TARGET = $(SRC:.bdf=.pcf)

.PHONY: all
all: $(TARGET)
	mkfontdir
	mkfontscale
	xset fp rehash
	@echo You may now restart the applications using this font.

%.pcf: %.bdf
	bdftopcf $^ -o $@
