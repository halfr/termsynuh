Fork of the `termsyn` 6x11 bitmap font.

I add the glyphs I need.

# Usage

In a file ran before you start applications, such as `.xinitrc`:

    xset +fp /path/to/termsynuh/
    xset fp rehash

## For urxvt

In your `Xresources` file:

    urxvt.font:             -*-termsynuh-medium-*-*-*-*-*-*-*-*-*-*-*
    urxvt.boldFont:         -*-termsynuh-bold-*-*-*-*-*-*-*-*-*-*-*

# Development

Requires `gbdfed` for editing gbd files and `xorg-bdftopcf`.

    $ gbdfed termsynuh6x11r.bdf # or termsynuh6x11b.bdf
    $ make
